<!doctype html>
<?php
require('mlib_functions.php');
require('mlib_values.php');
html_head("mlib status");
require('mlib_header.php');
require('mlib_sidebar.php');

# Code for your web page follows.
try
{
  //open database
  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>
 
<h2>Media Status</h2>
 <!-- Display media -->
    <table border=1>
      <tr>
        <td>Title</td><td>Author</td><td>Description</td><td>Type</td><td>Status</td><td>User</td><td>ReservedTill</td>
      </tr>

<?php
  $query = "SELECT * FROM media WHERE STATUS = 'active'";
  $result = $db->query($query);
  foreach($result as $row) {
    print "<tr>";
    print "<td>".$row['title']."</td>";
    print "<td>".$row['author']."</td>";
    print "<td>".$row['description']."</td>";
    print "<td>".$row['type']."</td>";
    print "<td>".$row['status']."</td>";
    	$user_id = $row['user_id'];
	if ($user_id > 0) {
		$result = $db->query("SELECT * FROM mlib_users where user_id = $user_id")->fetch();
		$user_name = $result['first']." ".$result['last'];
		$date_in = $row['date_in'];
	} else {
		$user_name = "available";
		$date_in = "not reserved";
	}
    print "<td>".$user_name."</td>";
    print "<td>".$date_in."</td>";
    print "</tr>";
  }

 print "</table>";

 // close connection
 $db = NULL;
}
catch(PDOExeption $e)
{
  echo 'Exception : '.$e->getMessage();
  echo "<br/>";
  $db = NULL;
}
require('mlib_footer.php');
?>
